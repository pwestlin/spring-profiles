package nu.westlin.springprofiles;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile(Profiles.TWO_WHEELS)
@Component
public class Motorcycle implements Vehicle {

    @Override public String type() {
        return this.getClass().getSimpleName();
    }
}
