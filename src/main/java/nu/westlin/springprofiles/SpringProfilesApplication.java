package nu.westlin.springprofiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("nu.westlin.springprofiles")
@EnableAutoConfiguration
@EnableConfigurationProperties
public class SpringProfilesApplication {

    static final Logger logger = LoggerFactory.getLogger(SpringProfilesApplication.class);

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(SpringProfilesApplication.class);
        ConfigurableApplicationContext ctx = springApplication.run(args);

        Vehicle vehicle = ctx.getBean(Vehicle.class);
        logger.info("vehicle.type() = {}", vehicle.type());
        ctx.stop();
    }

}
