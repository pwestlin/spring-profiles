package nu.westlin.springprofiles;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile(Profiles.FOUR_WHEELS)
@Component
public class Car implements Vehicle {

    @Override public String type() {
        return this.getClass().getSimpleName();
    }
}
