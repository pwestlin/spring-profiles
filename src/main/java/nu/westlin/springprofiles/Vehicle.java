package nu.westlin.springprofiles;

public interface Vehicle {
    String type();
}
