package nu.westlin.springprofiles;

public interface Profiles {

    public static final String TWO_WHEELS = "TwoWheels";
    public static final String FOUR_WHEELS = "FourWheels";
}
