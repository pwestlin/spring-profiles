package nu.westlin.springprofiles;

import nu.westlin.springprofiles.Motorcycle;
import nu.westlin.springprofiles.Profiles;
import nu.westlin.springprofiles.SpringProfilesApplication;
import nu.westlin.springprofiles.Vehicle;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SpringProfilesApplication.class)
@ActiveProfiles(Profiles.TWO_WHEELS)
public class SpringProfilesApplicationTwoWheelsTest {

    @Inject
    Vehicle vehicle;

    @Test
    public void foo() {
        assertTrue(vehicle instanceof Motorcycle);
    }
}