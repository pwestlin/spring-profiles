# README #

## Sammanfattning ##
Detta repository fungerar som en introduktion till hur [Spring Profiles](http://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-profiles.html) fungerar och hur man kan använda och testa m.h.a. av dem.

## Checka ut koden ##
> git clone https://pwestlin@bitbucket.org/pwestlin/spring-profiles.git

## Bygga och köra ##
> mvn clean test

## Vad är [Spring Profiles](http://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-profiles.html) ##
[Spring Profiles](http://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-profiles.html) är ett sätt att konfigurera en Spring-baserad applikation. Genom att skicka med vilka profiler som applikationen skall använda kan man styra ex. vilken databas eller JMS-buss som ska används. Man kan även styra om ex. Metrics ska köras över huvud taget.

## Förklaring av applikationen ##
Applikationen är mycket enkel. Den består av en "main-klass", _SpringProfilesApplication_, som är en [Spring Boot](http://projects.spring.io/spring-boot/)-klass som konfigurerar applikationen. Sedan finns interfacet _vehicle_ samt dess två implementationer - _Car_ och _Motorcycle_.

### Exekvera applikationen ###
Kör applikationen genom
> mvn spring-boot:run

Men varför får jag
> org.springframework.beans.factory.NoSuchBeanDefinitionException: No qualifying bean of type [nu.westlin.springprofiles.Vehicle] is defined?  


Jo, i _SpringProfilesApplication_ försöker vi göra 
>Vehicle vehicle = ctx.getBean(Vehicle.class)  

och det kräver att det finns en böna laddad i Spring-contextet som implementerar interfacet _Vehicle_.  
Men varför finns det inte det då?  
Det finns ju två klasser som implementerar _Vehicle_:  
* Car  
* Motorcycle  

Dessa är annoterade med **Profile(profilnamn)**. Detta innebär att klassen endast kommer laddas in i Spring-kontextet om aktiv profil matchar namnet profilnamnet på klassen. Vi startade ju applikationen utan att ange vilka profiler som skulle köras vilket ger detta fel.
#### Ange profiler ###
Detta gör man genom att skicka **-Dspring.profiles.active** som parameter till java.  
> mvn spring-boot:run **-Dspring.profiles.active=FourWheels**
Nu gick det bättre! Loggen ska skriva ut "vehicle.type() = Car".
Avbryt applikationen genom att trycka ctrl-c.
> mvn spring-boot:run **-Dspring.profiles.active=TwoWheels**
Loggen ska nu skriva ut "vehicle.type() = Motorcycle".
Avbryt applikationen genom att trycka ctrl-c.

### Tester ###
Applikationen har två tester, _SpringProfilesApplicationTwoWheelsTest_ och _SpringProfilesApplicationFourWheelsTest_.  
Dessa använder annoteringen **ActiveProfiles**(profilnamn) för att köra (alla) tester i klassen med fördefinierade profiler. 

## Vem ställer jag frågor till? ##
peter.westlin@gmail.com